// Help from Anna Peterson: https://javascript.plainenglish.io/the-worlds-easiest-the-rock-themed-rock-paper-scissors-javascript-tutorial-ee99b7f83e69

////////////// VARIABLES /////////////////////////

// Start overlay and form
const startGame = document.getElementById('startBtn');
const overlay = document.getElementById('startOverlay');
const playerName = document.getElementById('name');
const errMsg = document.getElementById('errMsg');
const form = document.getElementById('nameForm');
let weapon;

// Player name, computer name
const playerNameDisplay1 = document.getElementById('playerName1');
const playerNameDisplay2 = document.getElementById('playerName2');
const computerName = document.getElementById('computerName');
let searchTerm;

//Button IDs
let rockBtn = document.getElementById('rockButton'); 
let paperBtn = document.getElementById('paperButton'); 
let scissorsBtn = document.getElementById('scissorsButton'); 

// Weapons
let weapons = {
  rock: {defeats: 'scissors'},
  paper: {defeats: 'rock'},
  scissors: {defeats: 'paper'}
};

// Scores
const playerScoreText = document.getElementById('playerScoreText');
const tieScoreText = document.getElementById('tieScoreText');
const computerScoreText = document.getElementById('computerScoreText');
const playerScore = document.getElementById('playerScore');
const tieScore = document.getElementById('tieScore');
const computerScore = document.getElementById('computerScore');
let winner;
let results;
results = {
  player: 'rock',
  computer: 'rock'
};
let scores = {
  player: 0,
  tie: 0,
  computer: 0
};

let scoreText = {
  player: [playerScore, playerScoreText],
  computer: [computerScore,computerScoreText],
  tie: [tieScore, tieScoreText]
};

////////////// FUNCTIONS /////////////////////////

// Field input validation
const stringLengthValidation = (input) => {
  if ((input).length < 3) {
    errMsg.innerText = "Must be at least 3 characters";
    return false;
  } else {
    errMsg.innerText = " ";
    return true;
  }
}

// Overlay field name submit
form.addEventListener('submit', function(e) {
  e.preventDefault();
  if (!stringLengthValidation(playerName.value)) return false;
  const thankYouText = document.getElementById('thankYouText');
  thankYouText.innerText = `Awesome, let's play!`;

  // Timing function
  setTimeout(
    function () {
      overlay.style.display = 'none';
    },
    1200
  );

  // Field input display
  playerNameDisplay1.innerText = playerName.value;
  playerNameDisplay2.innerText = playerName.value;
});

// Random picker
function getRandomInt(max) {
  return Math.floor(Math.random() * Math.floor(max));
};

// Random computer weapon picker
const weaponPicker = () => {
  const num = getRandomInt(3);
  const weaponKeys = Object.keys(weapons);
  const weapon = weaponKeys[num];
  return weapon;
}

// Handling turn results
const handleResults = () => {
  results.computer = weaponPicker(); // Computer "chooses" weapon
  updateTextAndComputerImage();
  winner = determineWinner();
  console.log('turn complete!', results, winner);
  updateScores();
}

const updateTextAndComputerImage = () => {
  // Update player and computer text
  playerNameDisplay2.innerText = `${playerName.value} chose ${results.player}!`;
  computerName.innerText = `chose ${results.computer}!`;

  // Send computer "choice" to fetch image
  searchTerm = results.computer;
  fetchImagesComputer(searchTerm);
}

//Update scores
const updateScores = () => {
  scores[winner]++;
  playerScore.innerText = scores['player'];
  tieScore.innerText = scores['tie'];
  computerScore.innerText = scores['computer'];
  winnerTextColor(winner);
}

// Taking turns
const rockTurn = () => {
  results.player = 'rock';
  handleResults();
};

const paperTurn = () => {
  results.player = 'paper';
  handleResults();
};

const scissorsTurn = () => {
  results.player = 'scissors';
  handleResults();
};

// Determine winner
function determineWinner() {
  if (results.player === results.computer) {
    return 'tie';
  };
  if (weapons[results.player].defeats === results.computer) {
    return 'player';
  };
  if (weapons[results.computer].defeats === results.player) {
    return 'computer';
  };
};

// Highlight winner score/text
const winnerTextColor = (winner) => {
  scoreText[winner][0].style.color = 'red';
  scoreText[winner][1].style.color = 'red';
  setTimeout(
    function() {
      scoreText[winner][0].style.color = 'black';
      scoreText[winner][1].style.color = 'black';
    },
    1000
  );
}

// Fetch images for player
const fetchImagesPlayer = (searchTerm) => {
  fetch(`https://api.pexels.com/v1/search?per_page=10&orientation=landscape&query=${searchTerm}`, {
    headers: {
      Authorization: `${API_KEY}`
    }
  })
  .then(function(data) {
    return data.json();
  })
  .then(function(responseJson) {
    console.log(responseJson);
    displayResultsPlayer(responseJson);
  })
  .catch(function(err) {
    console.log(err);
  });
};

// Display player image
function displayResultsPlayer(responseJson) {
  let photoSrc = responseJson.photos[getRandomInt(10)].src.medium;
  let playerImg = document.getElementById('playerImg');
  playerImg.setAttribute('src', photoSrc);
};


// Fetch images for computer
const fetchImagesComputer = (searchTerm) => {
  fetch(`https://api.pexels.com/v1/search?per_page=10&orientation=landscape&query=${searchTerm}`, {
    headers: {
      Authorization: `${API_KEY}`
    }
  })
  .then(function(data) {
    return data.json();
  })
  .then(function(responseJson) {
    console.log(responseJson);
    displayResultsComputer(responseJson);
  })
  .catch(function(err) {
    console.log(err);
  });
};

// Display computer image
function displayResultsComputer(responseJson) {
  let photoSrc = responseJson.photos[getRandomInt(10)].src.medium;
  let computerImg = document.getElementById('computerImg');
  computerImg.setAttribute('src', photoSrc);
};


////////////// EVENT LISTENERS /////////////////////////

// Rock event listener
rockBtn.addEventListener('click', function(e) {
  e.preventDefault();
  rockTurn();
  searchTerm = 'rocks';
  fetchImagesPlayer(searchTerm);
});


// Paper event listener
paperBtn.addEventListener('click', function(e) {
  e.preventDefault();
  paperTurn();
  searchTerm = 'paper';
  fetchImagesPlayer(searchTerm);
});


// Scissors event listener
scissorsBtn.addEventListener('click', function(e) {
  e.preventDefault();
  scissorsTurn();
  searchTerm = 'scissors';
  fetchImagesPlayer(searchTerm);
});