# Rock, Paper, Scissors
A simple Javascript setup of the classic game: https://hardcore-kilby-aa9ec0.netlify.app/

## Motivation
This was the final project for the class, "Programming for the Browser with JavaScript", the first course in the Certificate in Full-Stack Development with JavaScript at the University of Washington, taught by Kevin Hyde.

## Built with:
- HTML
- CSS (some Bootstrap)
- Javascript

## Features:
- Form field validation
- Timing functions
- Fetch requests to the Pexels API
- Jasmine unit tests

## How to play:
It's pretty simple. Enter your name on the start screen and then click any of the rock/paper/scissors buttons to play! The computer's "choice" is randomly generated. With every round, a random image of the "weapon" will populate from the Pexels API and the winner will be briefly highlighted in red.