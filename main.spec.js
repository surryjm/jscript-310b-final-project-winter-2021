describe('tic-tac-toe tests', () => {
  it('should return false for string under 3 characters', () => {
    
    const stringLengthValidation = (input) => {
      if ((input).length < 3) {
        return false;
      } else {
        return true;
      };
    };

    let input1 = 'x';
    let result1 = stringLengthValidation(input1);
    expect(result1).toEqual(false);

    let input2 = 'xx';
    let result2 = stringLengthValidation(input2);
    expect(result2).toEqual(false);
  });


  it('should return false for a blank string', () => {
    
    const stringLengthValidation = (input) => {
      if ((input).length < 3) {
        return false;
      } else {
        return true;
      };
    };

    let input = '';
    let result = stringLengthValidation(input);
    expect(result).toEqual(false);
  });


  it('should return a tie', () => {
    results.player === 'rock';
    results.computer === 'rock';
    let result = determineWinner();
    expect(result).toEqual('tie');
  });

});


